#ifndef PCB_H
#define PCB_H

struct Pcb {
    int pid;
    int exekuziodenb;
};

extern struct Pcb lista[150];
extern int kop;

struct Pcb psortu();
void sartuFCFS();
void sartuSJF();

#endif // PCB_H