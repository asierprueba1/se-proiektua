#ifndef MACHINE_H
#define MACHINE_H
#include "pcb.h"

//Estrukturak
struct Thread {
    int tid;
    struct Pcb pcb;
    int okupatuta;
};

struct Core {
    struct Thread *threads;
    int h_kop;
};

struct CPU {
    struct Core *cores;
    int core_kop;
};

struct Machine {
    struct CPU *cpus;
    int cpu_kop;
};

#endif // MACHINE_H