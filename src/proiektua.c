#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include "pcb.h"
#include "machine.h"
#include "scheduler.h"

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;  // Mutexa erazagutu.
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond2 = PTHREAD_COND_INITIALIZER;
int tick_maiztasuna, temp_maiztasuna, schmaiztasuna, kkop, hkop, cpu_kop;
int tenp_kop = 2; //Tenporizadore Kopurua.
int kontatuta = 0; //Zenbat tenporizadoe seinalea hartu duten adierazten du.
int politika;
int elementuak = 0;
int kop = 0;
int tid = 1;
struct Pcb lista[150];
struct Machine makina;

// Erlojua
void *erlojua(void *arg) {
    int tick_maiztasuna = *((int *)arg);
    while (1) {
        //printf("Erlojua!\n");
        //sleep(tick_maiztasuna); // sleep funtzioa

        pthread_mutex_lock(&mutex); // Mutexa blokeatu
	while(kontatuta < tenp_kop){
        	pthread_cond_wait(&cond, &mutex); // Kondizionala itxaron

	}
	kontatuta = 0;
	pthread_cond_broadcast(&cond2);
        pthread_mutex_unlock(&mutex); // Mutexa askatu

    }
    return NULL;
}

// PCB Tenporizadorea
void *tenporizadoreapcb(void *arg) {
    int temp_maiztasuna = *((int *)arg);
    int kont = 0;
    pthread_mutex_lock(&mutex);
    printf("Politika: %d\n", politika);
    while (1) {
	kont++;
    kontatuta++;
	if(kont == temp_maiztasuna){
        if(politika == 1){
            sartuFCFS();
        }else{
            sartuSJF();
        }
		//printf("PCB sortuta eta eta ilaran sartuta: \n");
		kont = 0;
	}
	pthread_cond_signal(&cond);//kondizioa
	pthread_cond_wait(&cond2, &mutex);
    }
    return NULL;
}

// Scheduler Tenporizadorea
void *tenporizadoreasch(void *arg) {
    
    int konts = 0;
    pthread_mutex_lock(&mutex);
    while (1) {
        konts++;
        kontatuta++;
        if(konts == temp_maiztasuna){
            //printf("SCH_Tick\n"); 
            Scheduler(makina);
            konts = 0;
        }
    pthread_cond_signal(&cond);//kondizioa
    pthread_cond_wait(&cond2, &mutex);
    }
    return NULL;
}


int main() {

    pthread_t sch_thread, pcb_thread;
    pthread_t erlojua_thread;
    
    printf("Zenbat CPU nahi dituzu makinan?\n");
    scanf("%d", &cpu_kop);
    printf("Zenbat kore nahi dituzu CPU-rako?\n");
    scanf("%d", &kkop);
    printf("Eta hari koreko?\n");
    scanf("%d", &hkop);
    printf("Aukeratu Politka\n");
    printf("Sakatu '1' FCFS aukeratzeko edo '2' SJF aukeratzeko\n");
    scanf("%d", &politika);
    printf("Ze maiztasuna nahi duzu erlojurako? (segundutan)\n");
    scanf("%d", &tick_maiztasuna);
    printf("Ze maiztasun nahi duzu pcb tenporizadorearentzat?\n");
    scanf("%d", &temp_maiztasuna);
    printf("Ze maiztasun nahi duzu scheduler tenporizadorearentzat?\n");
    scanf("%d", &schmaiztasuna);

    makina.cpus= (struct CPU*)malloc(sizeof(struct CPU)*cpu_kop);
    for(int i = 0; i < cpu_kop; i++){
        makina.cpus[i].cores =(struct Core*)malloc(sizeof(struct Core)*kkop);
        for(int j = 0; j < kkop; j++){
            makina.cpus[i].cores[j].threads =(struct Thread*)malloc(sizeof(struct Thread)*hkop);
            for(int z = 0; z < hkop; z++){
                makina.cpus[i].cores[j].threads[z].okupatuta = 0;
                makina.cpus[i].cores[j].threads[z].tid = tid;
                tid++;
                makina.cpus[i].cores[j].threads[z].pcb.pid = 0;
            }

        }
    }
    makina.cpu_kop = cpu_kop;
    makina.cpus->core_kop = kkop;
    makina.cpus->cores->h_kop=hkop;

    struct Sch *sch;
   
    pthread_create(&erlojua_thread, NULL, erlojua, &tick_maiztasuna);
    pthread_create(&pcb_thread, NULL, tenporizadoreapcb, &temp_maiztasuna);
    pthread_create(&sch_thread, NULL, tenporizadoreasch, (void*)sch);

    pthread_join(erlojua_thread, NULL);
    for(int i = 0; i < tenp_kop; i++){
	pthread_join(i, NULL);
    }

    pthread_mutex_destroy(&mutex); // Mutexa amaitu

    return 0;
}
