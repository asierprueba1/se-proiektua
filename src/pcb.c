#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include "pcb.h"


int pid=1;
//Prozesua sortzeko
struct Pcb psortu() {
	struct Pcb pcbBerri;
    pcbBerri.pid=pid;
    pid++;
    if(pid == 10000){
        pid=1;
    }
    pcbBerri.exekuziodenb = (rand() % 10) +1;
	return pcbBerri;
};

void sartuFCFS() {
    if(kop >= 149){
        printf("Ezin da sartu\n");
    }else{
        struct Pcb pcbBerri = psortu();
        if(kop == 0){
            lista[0] = pcbBerri;
        }else{
            lista[kop] = pcbBerri;
            
        }
        kop++;
        /*for (int i = 0; i < kop; i++) {
            printf("Listaren %d elementua: %d, eta exekdenb = %d\n", i, lista[i].pid, lista[i].exekuziodenb);
        }
        printf("\n");*/
    }
}

void sartuSJF() {
    if (kop >= 149) {
        printf("Ezin da sartu\n");
    } else {
        struct Pcb pcbBerri = psortu();
        int pos = kop - 1;
        
        while (pos >= 0 && lista[pos].exekuziodenb > pcbBerri.exekuziodenb) {
            lista[pos + 1] = lista[pos];
            pos--;
        }
        
        lista[pos + 1] = pcbBerri;
        kop++;

        /*for (int i = 0; i < kop; i++) {
            printf("Listaren %d elementua: %d, eta exekdenb = %d\n", i, lista[i].pid, lista[i].exekuziodenb);
        }
        printf("\n");*/
    }
}